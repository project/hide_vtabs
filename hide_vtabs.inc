<?php
/**
 * @file
 * Helper functions to handle role information.
 */

/**
 * Get which roles are selected to mark the default checkboxes on form load.
 *
 * @param string $node_type
 *   Type of the current content type.
 * @param array $roles
 *   An array whose keys are the role names.
 *
 * @return array
 *   Returns the array with the previously selected roles ID.
 */
function _hide_vtabs_default_roles_content_type($node_type, $roles) {
  $default_roles = array();
  foreach ($roles as $role_name) {
    $role_value = variable_get('hide_vtab_node_' . $node_type . '_' . str_replace(' ', '_', $role_name));
    if ((isset($role_value)) && ($role_value > 0)) {
      $role = user_role_load_by_name($role_name);
      $default_roles[] = $role->rid;
    }
  }
  return $default_roles;
}

/**
 * Retrieve role name from role ID (rid).
 *
 * @param string $rid
 *   Role ID value.
 *
 * @return array
 *   Returns the role name.
 */
function _hide_vtabs_retrieve_role_name($rid) {
  $role = user_role_load($rid);
  $role_name = str_replace(' ', '_', $role->name);
  return $role_name;
}
